package ru.fadeev.tm.context;

import ru.fadeev.tm.command.CommandExecutor;
import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.repository.ProjectRepository;
import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.Helper;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Bootstrap {

    private Map<String, CrudAble> services = new LinkedHashMap<>();

    private CommandExecutor commandExecutor = new CommandExecutor();

    private TaskRepository taskRepository = new TaskRepository();

    private ProjectRepository projectRepository = new ProjectRepository();

    public void init() {
        servicesInit();
        commandExecutor.init(this);
        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Operation operation = null;
        do {
            try {
                String[] managerAndCommand = Helper.getManagerAndCommand();
                operation = Operation.getAllowableOperation(managerAndCommand[1]);
                commandExecutor.execute(operation, getServiceByName(managerAndCommand[0]));
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        } while (operation != Operation.EXIT);
    }

    public CrudAble getServiceByName(String serviceName) {
        if (serviceName == null || serviceName.isEmpty())
            return null;
        return services.get(serviceName);
    }

    public Set<String> getAllServicesName() {
        return services.keySet();
    }

    private void servicesInit() {
        services.put("project", new ProjectService(projectRepository, taskRepository));
        services.put("task", new TaskService(taskRepository));
    }

}