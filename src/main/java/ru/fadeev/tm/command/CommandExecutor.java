package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.service.CrudAble;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {

    private Map<Operation, AbstractCommand> allKnownCommandsMap = new HashMap<>();

    public void execute(Operation operation, CrudAble service) {
        if (operation != null) {
            if (allKnownCommandsMap.containsKey(operation))
                allKnownCommandsMap.get(operation).execute(service);
        }
    }

    public void init(Bootstrap bootstrap) {
        allKnownCommandsMap.put(Operation.CREATE, new ProjectCreateCommand(bootstrap));
        allKnownCommandsMap.put(Operation.LIST, new ListCommand(bootstrap));
        allKnownCommandsMap.put(Operation.HELP, new HelpCommand(bootstrap));
        allKnownCommandsMap.put(Operation.REMOVE, new RemoveCommand(bootstrap));
        allKnownCommandsMap.put(Operation.CLEAR, new ClearCommand(bootstrap));
        allKnownCommandsMap.put(Operation.EXIT, new ExitCommand(bootstrap));
        allKnownCommandsMap.put(Operation.EDIT, new EditCommand(bootstrap));
        allKnownCommandsMap.put(Operation.PROJECT_TASKS, new ProjectTasksCommand(bootstrap));
        allKnownCommandsMap.put(Operation.PROJECT_ADD, new TaskProjectCommand(bootstrap));
    }

}