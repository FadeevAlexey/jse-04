package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.util.Helper;

public class TaskProjectCommand extends AbstractCommand {

    public TaskProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        System.out.println("[ADD TASK TO PROJECT]");
        System.out.println("ENTER TASK NAME");
        String taskName = Helper.readString();
        System.out.println("ENTER PROJECT NAME");
        String projectName = Helper.readString();
        ProjectService projectService = (ProjectService) service;
        projectService.addIdToTask(taskName, projectName);
        System.out.println("[OK]\n");
    }

}
