package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.service.CrudAble;

public class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        System.out.println("Thank you for using our Task Manager");
    }

}