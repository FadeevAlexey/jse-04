package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.service.CrudAble;

public class HelpCommand extends AbstractCommand {

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        for (Operation operation : Operation.values()) {
            if (operation.getDescription().matches("%s:.*"))
                System.out.println(String.format(operation.getDescription()
                        , operation.name().toLowerCase()).replaceAll("_", "-"));
        }
        for (String crudAble : this.bootstrap.getAllServicesName()) {
            for (Operation operation : Operation.values()) {
                if (operation.getDescription().matches("%s-%s:.*"))
                    System.out.println(
                            String.format(operation.getDescription()
                                    , crudAble, operation.name().toLowerCase(), crudAble));
            }
        }
        System.out.println();
    }

}