package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.Helper;

public class RemoveCommand extends AbstractCommand {

    public RemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s REMOVE]", service.getName().toUpperCase()));
        System.out.println("ENTER NAME");
        String name = Helper.readString();
        service.remove(name);
        System.out.println("[OK]\n");
    }

}
