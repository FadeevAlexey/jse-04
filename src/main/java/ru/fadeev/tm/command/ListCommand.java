package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.IEntity;
import ru.fadeev.tm.service.CrudAble;

import java.util.List;

public class ListCommand extends AbstractCommand {

    public ListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s LIST]", service.getName().toUpperCase()));
        List<IEntity> entities = service.list();
        for (int i = 0; i < entities.size(); i++) {
            System.out.println((i + 1) + ". " + entities.get(i));
        }
        System.out.println();
    }

}