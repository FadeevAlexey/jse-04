package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.Helper;

import java.util.Date;

public class EditCommand extends AbstractCommand {

    public EditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s EDIT]", service.getName().toUpperCase()));
        System.out.println("ENTER NAME:");
        String name = Helper.readString();
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = Helper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        Date startDate = Helper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        Date finishDate = Helper.readDate();
        service.edit(name, description, startDate, finishDate);
        System.out.println("[OK]\n");
    }

}