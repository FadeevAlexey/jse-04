package ru.fadeev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() {
        String string = null;
        try {
            string = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }

    public static String[] getManagerAndCommand() {
        String[] result = readString().split("-");
        if (result.length == 1)
            return new String[]{null, result[0]};
        return result;
    }

    public static Date readDate() {
        String stringDate = readString();
        if (stringDate.isEmpty())
            return null;
        else if (stringDate.matches("\\d{2}.\\d{2}.\\d{4}")) {
            try {
                return DATE_FORMAT.parse(stringDate);
            } catch (ParseException ignored) {
            }
        }
        System.out.println("Wrong format date, please try again");
        return readDate();
    }

    public static String dateToString(Date date) {
        if (date == null)
            return "";
        return DATE_FORMAT.format(date);
    }

    public static void checkName(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException();
    }

}