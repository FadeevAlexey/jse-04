package ru.fadeev.tm.repository;

import ru.fadeev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    public List<Task> findAll() {
        return new LinkedList<>(tasks.values());
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public void remove(String id) {
        tasks.remove(id);
    }

    public void remove(Task task) {
        tasks.remove(task.getId());
    }

    public void persist(Task task) {
        if (tasks.containsKey(task.getId()))
            throw new IllegalArgumentException("task with same id already exist");
        tasks.put(task.getId(), task);
    }

    public void merge(Task task) {
        if (!tasks.containsKey(task.getId()))
            tasks.put(task.getId(), task);
        else {
            Task mutable = tasks.get(task.getId());
            mutable.setName(task.getName());
            mutable.setProjectId(task.getProjectId());
            mutable.setDescription(task.getDescription());
            mutable.setFinishDate(task.getFinishDate());
            mutable.setStartDate(task.getStartDate());
        }
    }

    public void removeAll() {
        tasks = new LinkedHashMap<>();
    }

}
