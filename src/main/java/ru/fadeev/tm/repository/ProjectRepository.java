package ru.fadeev.tm.repository;

import ru.fadeev.tm.entity.Project;

import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    public List<Project> findAll() {
        return new LinkedList<>(projects.values());
    }

    public Project findOne(String id) {
        return projects.get(id);
    }

    public void remove(String id) {
        projects.remove(id);
    }

    public void remove(Project project){
        projects.remove(project.getId());
    }

    public void persist(Project project) {
        if (projects.containsKey(project.getId()))
            throw new IllegalArgumentException("project with same id already exist");
        projects.put(project.getId(), project);
    }

    public void merge(Project project) {
        if (!projects.containsKey(project.getId()))
            projects.put(project.getId(), project);
        else {
            Project mutable = projects.get(project.getId());
            mutable.setName(project.getName());
            mutable.setDescription(project.getDescription());
            mutable.setFinishDate(project.getFinishDate());
            mutable.setStartDate(project.getStartDate());
        }
    }

    public void removeAll() {
        projects = new LinkedHashMap<>();
    }

}