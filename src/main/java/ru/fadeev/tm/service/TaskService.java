package ru.fadeev.tm.service;

import ru.fadeev.tm.entity.IEntity;
import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.util.Helper;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TaskService implements CrudAble {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        Helper.checkName(name);
        Task task = new Task();
        task.setName(name);
        taskRepository.persist(task);
    }

    @Override
    public List<IEntity> list() {
        return new LinkedList<>(taskRepository.findAll());
    }

    @Override
    public void clear() {
        taskRepository.removeAll();
    }

    @Override
    public void remove(String taskName) {
        Helper.checkName(taskName);
        String id = findIdByName(taskName);
        if (id == null)
            throw new IllegalArgumentException("Can't remove task with name: " + taskName);
        taskRepository.remove(id);
    }

    @Override
    public void edit(String name, String description, Date startDate, Date finishDate) {
        Helper.checkName(name);
        String id = findIdByName(name);
        if (id == null)
            throw new IllegalArgumentException("can't find task with name: " + name);
        Task updatable = taskRepository.findOne(id);
        if (description != null && !description.isEmpty()) updatable.setDescription(description);
        if (startDate != null) updatable.setStartDate(startDate);
        if (finishDate != null) updatable.setFinishDate(finishDate);
        taskRepository.merge(updatable);
    }


    public String findIdByName(String name) {
        Optional<Task> task = taskRepository.findAll()
                .stream()
                .filter(tsk -> tsk.getName().equals(name))
                .findAny();
        return task.map(Task::getId).orElse(null);
    }

    @Override
    public String getName() {
        return "Task";
    }

}