package ru.fadeev.tm.service;

import ru.fadeev.tm.entity.IEntity;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.repository.ProjectRepository;
import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.util.Helper;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectService implements CrudAble {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String projectName) {
        Helper.checkName(projectName);
        Project project = new Project();
        project.setName(projectName);
        projectRepository.persist(project);
    }

    @Override
    public List<IEntity> list() {
        return new LinkedList<>(projectRepository.findAll());
    }

    @Override
    public void clear() {
        projectRepository.removeAll();
        taskRepository
                .findAll()
                .forEach(task -> {
                    if (!task.getProjectId().isEmpty())
                        taskRepository.remove(task);
                });
    }

    @Override
    public void remove(String name) {
        Helper.checkName(name);
        String projectId = findIdByName(name);
        projectRepository.remove(projectId);
        taskRepository.findAll()
                .stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> taskRepository.remove(task));
    }

    @Override
    public void edit(String name, String description, Date startDate, Date finishDate) {
        Helper.checkName(name);
        String id = findIdByName(name);
        if (id == null)
            throw new IllegalArgumentException("can't find project with name: " + name);
        Project updatable = projectRepository.findOne(id);
        if (description != null && !description.isEmpty()) updatable.setDescription(description);
        if (startDate != null) updatable.setStartDate(startDate);
        if (finishDate != null) updatable.setFinishDate(finishDate);
        projectRepository.merge(updatable);
    }

    public List<Task> getProjectTasks(String name) {
        Helper.checkName(name);
        return taskRepository.findAll()
                .stream()
                .filter(task -> task.getProjectId().equals(findIdByName(name)))
                .collect(Collectors.toList());
    }

    public void addIdToTask(String taskName, String projectName) {
        Helper.checkName(taskName);
        Helper.checkName(projectName);
        taskRepository.findAll()
                .stream()
                .filter(tsk -> tsk.getName().equals(taskName))
                .findAny()
                .ifPresent(value -> value.setProjectId(findIdByName(projectName)));
    }

    public String findIdByName(String name) {
        Optional<Project> desireProject = projectRepository
                .findAll()
                .stream()
                .filter(x -> x.getName().equals(name))
                .findAny();
        if (desireProject.isPresent())
            return desireProject.get().getId();
        throw new IllegalArgumentException("can't find project");
    }

    @Override
    public String getName() {
        return "Project";
    }

}