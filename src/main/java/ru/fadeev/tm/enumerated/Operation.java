package ru.fadeev.tm.enumerated;

public enum Operation {

    CREATE("%s-%s: Create new %s"),
    CLEAR("%s-%s: Remove all %ss"),
    LIST("%s-%s: Show all %ss"),
    REMOVE("%s-%s: Remove selected %s"),
    HELP("%s: Show all commands."),
    EXIT("%s: Log out of system."),
    EDIT("%s-%s: Edit %s description"),
    PROJECT_TASKS("%s: Show all tasks inside project"),
    PROJECT_ADD("%s: Adding tasks to the project");

    private String description;

    Operation(String string) {
        this.description = string;
    }

    public String getDescription() {
        return description;
    }

    public static Operation getAllowableOperation(String operation) {
        switch (operation.toLowerCase()) {
            case "create":
                return Operation.CREATE;
            case "clear":
                return Operation.CLEAR;
            case "list":
                return Operation.LIST;
            case "remove":
                return Operation.REMOVE;
            case "help":
                return Operation.HELP;
            case "exit":
                return Operation.EXIT;
            case "edit":
                return Operation.EDIT;
            case "tasks":
                return Operation.PROJECT_TASKS;
            case "add":
                return Operation.PROJECT_ADD;
            default:
                throw new IllegalArgumentException("wrong command name");
        }
    }

}